# **Especificação de Caso de Uso: Cadastrar/Alterar Parecer da Comissão**

## **1. Descrição do Caso de Uso**

- Caso de uso responsável por realizar o cadastro dos pareceres de vaga docente.

- O cadastro dos pareceres é realizado após submissão das novas vagas docente por parte dos departamentos/unidades acadêmicas especializadas. Neste ponto, os gestores de banco de vagas deverão proceder com o deferimento ou indeferimento das solicitações de vagas pendentes de análise.

## **2. Pré-condições**

- O ator deve estar logado no sistema como gestor de banco de vagas ou como gestor editais convocação para ter acesso a funcionalidade.

## **3. Fluxo de Eventos**

### **3.1. Fluxo Principal**

1. O sistema inclui o caso de uso: *Analisar Solicitação de Vaga Docente*;
1. O ator seleciona a opção *Cadastrar/Alterar Parecer da Comissão*;
1. O sistema apresenta a listagem com informações da solicitação de vaga docente;
1. O ator informa os dados do parecer e aciona o cadastro do parecer de vaga docente;
1. O caso de uso é encerrado.

### **3.2. Fluxo Alternativo:**

Não se aplica.

### **3.3. Fluxo de Exceções**

#### FE01

Não se aplica.

## **4. Blocos de Dados**

### BD01

**Listagem com informações da solicitação de vaga docente.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Situação                              | N/A                      | N/A          | S            | Situação da solicitação de vaga docente.
Unidade               	              | N/A                      | N/A          | S            | Unidade solicitante da vaga docente.              
Submissão realizada para edital aberto entre  | N/A              | N/A          | S            | Período oficial de solicitação de vagas docente definido em edital.
Data de Cadastro   	                  | N/A                      | N/A          | S            | Data de cadastro da solicitação de vaga docente
Justificativa                         | N/A                      | N/A          | S            | Justificativa para submissão das vagas docentes.
Plano de Redução de Vagas             | N/A                      | N/A          | S            | Plano de redução de vagas do departamento/unidade acadêmica especializada. Campo é exibido apenas quando informado durante a solicitação de vaga docente.

### BD02

**Listagem com informações da área de contratação selecionada.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Tipo de Demanda                       | N/A                      | N/A          | S            | Tipo de demanda solicitada.
Regime de Trabalho 	   	              | N/A                      | N/A          | S            | Regime de trabalho informado.
Área do Concurso	                    | N/A                      | N/A          | S            | Área do concurso informada.
Classe Funcional	                    | N/A                      | N/A          | S            | Classe funcional da vaga informada.

### BD03

**Listagem com informações da justificativa/comprovação da demanda.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Tipo de Justificativa                 | N/A                      | N/A          | S            | Tipo de justificativa para a demanda informada.
Justificativa para a demanda          | N/A                      | N/A          | S            | Justificativa para a demanda informada.

### BD04

**Listagem com informações do banco de equivalentes.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Uso do Banco/Teto                     | N/A                      | N/A          | S            | Indica a relação entre o uso atual do banco de equivalentes e o seu teto máximo.
Saldo                                 | N/A                      | N/A          | S            | Saldo atual do banco de equivalentes.
Saldo a Ocupar/Reservado              | N/A                      | N/A          | S            | Salto a ocupar/reservado no banco de equivalentes.
Saldo Previsto                        | N/A                      | N/A          | S            | Saldo final previsto para o banco de equivalentes.
Vagas Disponíveis no Edital           | N/A                      | N/A          | S            | Vagas associadas ao edital de distribuição de vagas.
Período de Vigência                   | N/A                      | N/A          | S            | Data da última atualização do teto de vagas.
Peso da solicitação                   | N/A                      | N/A          | S            | Peso da solicitação de vaga docente.  

### BD05

**Listagem com informações dos fatores de vagas.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Descrição	                            | N/A                      | N/A          | S            | Descrição do fator de vaga.
Fator	                                | N/A                      | N/A          | S            | Fato de vaga.  
Cargo	                                | N/A                      | N/A          | S            | Cargo associado ao fator de vaga.
Regime de Trabalho	                  | N/A                      | N/A          | S            | Regime de trabalho do fator de vaga associado.
Restringido às Situações Funcionais   | N/A                      | N/A          | S            | Restrição funcional do fator de vaga.

### BD06

**Formulário para cadastro do parecer de vaga docente.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Parecer da Comissão                   | TEXTO                    | S            | E            | Parecer de vaga docente.
Situação                              | SELEÇÃO ÚNICA            | S            | E            | Situação de aprovação do parecer. São apresentadas as opções: `Autorizado` e `Negado`.
Situação da Vaga	                    | SELEÇÃO ÚNICA            | S            | E            | Status futuro da vaga docente.
Vaga do Edital                        | SELEÇÃO ÚNICA            | S            | E            | Vaga do edital. São apresentados os códigos associados ao edital com status: `Vaga Desocupada/Reservada para Distribuição`.
Regime de Trabalho Autorizado	        | SELEÇÃO ÚNICA            | S            | E            | Regime de trabalho autorizado para a solicitação de vaga.
Anexo do Parecer	                    | ARQUIVO                  | N            | E            | Anexo do parecer de vaga docente.

N/A= Não se Aplica; E = Entrada; S= Saída; E/S= Entrada e Saída; Obrigatório: Sim = S; Não = N.

## **5. Requisitos Legais**

### RL01

1. Resolução CONSEPE 110/2018, de 10 de junho de 2008 - Aprova normas de gerenciamento do Banco de Professor-Equivalente da UFRN e critérios para distribuição de vagas docentes.

### RL02

1. Decreto 8259/2014, de 29 de maio de 2014 - Altera o Decreto nº 7.485, de 18 de maio de 2011, que dispõe sobre a constituição de banco de professor-equivalente das universidades federais vinculadas ao Ministério da Educação, e altera o Decreto no 7.312, de 22 de setembro de 2010, que dispõe sobre o banco de professor-equivalente de educação básica, técnica e tecnológica dos Institutos Federais de Educação, Ciência e Tecnologia, vinculados ao Ministério da Educação.

## **6. Regras de Negócio**

#### RN01

1. Durante o deferimento do parecer de vaga docente, apenas os códigos de vaga associadas ao edital de distribuição de vagas com status: `Vaga Desocupada/Reservada para Distribuição` estarão disponíveis para seleção.
1. Durante o deferimento do parecer de vaga docente, caso a situação da vaga selecionada seja do tipo: `Vaga Reservada para Alteração de RT` não será possível associar um código de vaga desocupado junto ao parecer de vaga docente.
1. Após deferimento do parecer de vaga docente, o código de vaga associado terá seu status atualizado para a situação definida durante o deferimento. Em paralelo, também é realizada uma movimentação na vaga informada. Os status possíveis são esses: `Vaga Desocupada/Reservada para Concurso`, `Vaga Desocupada/Reservada para Aproveitamento`, `Vaga Desocupada/Reservada para Redistribuição` e `Vaga Desocupada/Reservada para Remoção`.
1. O saldo do banco de equivalentes se utiliza da seguinte fórmula: `SALDO = TETO DO BANCO DE VAGAS - TOTAL DE VAGAS OCUPADAS`. Neste ponto, o saldo total é calculado em cima da diferença entre o saldo limite do teto de vagas mais atual (referente ao teto de banco mais recente cadastrado) e o total de servidores docentes do magistério superior ativos na instituição (Cada categoria de docente possui um fator de vaga específico no teto de vagas).
1. O saldo total previsto para o banco de equivalentes se utiliza da seguinte fórmula: `SALDO DISPONÍVEL = SALDO - SALDO A OCUPAR/RESERVADO`. Neste ponto, o saldo previsto é calculado em cima da diferença entre o saldo atual e o total de solicitações autorizadas (São consideradas solicitações autorizadas as solicitações que ocupam ou não códigos de vagas).
1. A solicitação vaga docente só será autorizada caso o valor associado a autorização não ultrapasse o limite máximo de saldo disponível vigente. Essa verificação é realizada conforme a fórmula: `SALDO DISPONÍVEL ≥ FATOR DA NOVA VAGA`.

## **7. Documentos Relacionados**
- Glossário do módulo de férias (GLO_SIGRH_Férias.md);
- Mensagens do módulo de férias (MSG_SIGRH_Férias.md).
