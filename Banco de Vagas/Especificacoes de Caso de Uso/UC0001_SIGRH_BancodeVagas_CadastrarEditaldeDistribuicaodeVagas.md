# **Especificação de Caso de Uso: Cadastrar Edital de Distribuição de Vagas**

## **1. Descrição do Caso de Uso**

- Caso de uso responsável por realizar o cadastro do edital de distribuição de vagas docente.

- O edital de distribuição de vagas estabelece as diretrizes para submissão de novas vagas docente via sistema. Durante o período estabelecido no edital de distribuição de vagas, os gestores das unidades acadêmicas (departamentos e/ou unidades acadêmicas especializadas) poderão realizar o cadastro das solicitações de novas vagas docente, apresentando as devidas justificativas e necessidades para cada demanda de reposição de vagas.

## **2. Pré-condições**

- O ator deve estar logado no sistema como gestor de banco de vagas ou como gestor editais convocação para ter acesso a funcionalidade.

## **3. Fluxo de Eventos**

### **3.1. Fluxo Principal**

1. O ator seleciona a opção *Cadastrar Edital de Distribuição de Vagas*; **[[RN01.6](#rn01)]**
1. O sistema apresenta os campos necessários para cadastro das informações do edital; **[[BD01](#bd01)]**
1. O ator informa os dados do edital e aciona a inserção de vagas desocupadas; **[[BD01](#bd01)]**, **[[RN01.1](#rn01)]**, **[[RN01.3](#rn01)]**, **[[RN01.4](#rn01)]**
1. O sistema apresenta os campos necessários para consulta das vagas desocupadas; **[[BD02](#bd02)]**
1. O ator informa os dados para consulta e aciona a consulta de vagas desocupadas; **[[BD02](#bd02)]**,
1. O sistema apresenta a listagem de vagas desocupadas; **[[BD03](#bd03)]**, **[[FA01](#fa01)]**
1. O ator adiciona vagas ao cadastro do edital; **[[BD03](#bd03)]**, **[[RN01.2](#rn01)]**
1. O sistema apresenta as vagas adicionadas; **[[BD04](#bd04)]**, **[[FA01](#fa01)]**
1. O ator aciona o cadastro do edital de distribuição de vagas; **[[FE01](#fe01)]**, **[[FE02](#fa02)]**, **[[FA03](#fa03)]**, **[[RN01.5](#rn01)]**, **[[MSG0001]]**, **[[MSG0002]]**, **[[MSG0003]]**
1. O caso de uso é encerrado.

### **3.2. Fluxo Alternativo:**

#### FA01

**Visualizar detalhamento da vaga.**

1. O ator realiza a consulta de vagas desocupadas; **[[BD02](#bd02)]**
1. O sistema apresenta a listagem de vagas desocupadas; **[[BD03](#bd03)]**
1. O ator aciona a operação *Visualizar Detalhes da Vaga*;
1. O sistema estende o caso de uso **[[Visualizar Detalhes da Vaga](#VisualizarDetalhesdaVaga)]**;
1. O caso de uso é encerrado.

### **3.3. Fluxo de Exceções**

#### FE01

**Cadastro de editais concomitantes.**

1. O ator realiza o cadastro de um novo edital com período concomitante junto a outro edital; **[[BD01](#bd01)]**, **[[RN01.1](#rn01)]**
2. O sistema exibe mensagem de validação; **[[MSG0001]]**
3. O caso de uso é encerrado.

#### FE02

**Inconsistência no cadastro da data de término de exibição do parecer.**

1. O ator realiza o cadastro de um novo edital com a data de exibição do parecer anterior a data término do edital; **[[BD01](#bd01)]**, **[[RN01.3](#rn01)]**
2. O sistema exibe mensagem de validação;  **[[MSG0002]]**
3. O caso de uso é encerrado.

#### FE03

**Cadastro de editais sem vagas adicionadas**

1. O ator realiza o cadastro de um novo edital sem vagas adicionadas; **[[BD01](#bd01)]**, **[[RN01.4](#rn01)]**
2. O sistema exibe mensagem de validação; **[[MSG0003]]**
3. O caso de uso é encerrado.

#### FE04

**Teto de banco de vagas não vigente.**

1. O ator acessa a funcionalidade sem a existência de teto de banco de vagas vigente no atual momento; **[[RN01.8](#rn01)]**
2. O sistema exibe mensagem de validação; **[MSG006]**
3. O caso de uso é encerrado.

## **4. Blocos de Dados**

### BD01

**Formulário para cadastro do edital de distribuição de vagas.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Tipo do Edital                        | SELEÇÃO ÚNICA            | S            | E            | Indica o tipo de edital a ser cadastrado. São apresentadas as opções: `Edital exclusivo para alteração de regime de carga horária`, `Edital exclusivo para reposição de novas vagas de docente` e `Sem restrições para cadastro de solicitação`. A opção: `Edital exclusivo para reposição de novas vagas de docente` já vem selecionada por padrão.
Descrição                             | TEXTO                    | S            | E            | Descrição do edital. Limite de caracteres: 320.
Data de Início                        | DATA                     | S            | E            | Data início para submissão das novas vagas docentes. Máscara: DD/MM/AAAA.
Data de Término                       | DATA                     | S            | E            | Data término para submissão das novas vagas docentes. Máscara: DD/MM/AAAA.  
Data de Exibição do Parecer           | DATA                     | S            | E            | Data de exibição dos pareceres de vaga docente junto aos departamentos. Máscara: DD/MM/AAAA.
Adicionar Vagas Desocupadas           | IMAGEM                   | S/N          | E            | Possibilita a adição de vagas desocupadas junto ao cadastro do edital. Campo não é exibido caso a opção: `Edital exclusivo para reposição de novas vagas de docente` esteja selecionada.

### BD02

**Formulário para consulta das vagas desocupadas.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Código                                | NUMÉRICO                 | N            | E            | Código da vaga a ser consultada.
Origem da Vaga                        | SELEÇÃO ÚNICA            | N            | E            | Origem da vaga a ser consultada.
Cargo                                 | SELEÇÃO ÚNICA            | N            | E            | Cargo da vaga a ser consultada.
Classe Funcional                      | SELEÇÃO ÚNICA            | N            | E            | Classe funcional da vaga a ser consultada.

### BD03

**Listagem de vagas desocupadas.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Código                                | N/A                      | N/A          | S            | Código da vaga consultada.
Tipo de Vaga                          | N/A                      | N/A          | S            | Tipo da vaga consultada.
Origem da Vaga                        | N/A                      | N/A          | S            | Origem da vaga consultada.
Cargo                                 | N/A                      | N/A          | S            | Cargo da vaga consultada.
Classe Funcional                      | N/A                      | N/A          | S            | Classe funcional da vaga consultada.
Situação                              | N/A                      | N/A          | S            | Situação da vaga consultada.
Adicionar Vaga                        | IMAGEM                   | N            | E            | Possibilita a adição da vaga junto ao cadastro do edital.
Visualizar Detalhes da Vaga           | IMAGEM                   | N            | E            | Possibilita a visualização dos detalhes da vaga adicionada.
Remover Vaga                          | IMAGEM                   | N            | E            | Possibilita a remoção da vaga adicionada junto ao cadastro do edital.


### BD04

**Listagem de vagas adicionadas.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Código                                | N/A                      | N/A          | S            | Código da vaga adicionada.
Tipo de Vaga                          | N/A                      | N/A          | S            | Tipo da vaga adicionada.
Origem da Vaga                        | N/A                      | N/A          | S            | Origem da vaga adicionada.
Cargo                                 | N/A                      | N/A          | S            | Cargo da vaga adicionada.
Classe Funcional                      | N/A                      | N/A          | S            | Classe funcional da vaga adicionada.
Situação                              | N/A                      | N/A          | S            | Situação da vaga adicionada.
Visualizar Detalhes da Vaga           | IMAGEM                   | N            | E            | Possibilita a visualização dos detalhes da vaga adicionada. **[[FA01](#fa01)]**
Remover Vaga                          | IMAGEM                   | N            | E            | Possibilita a remoção da vaga adicionada junto ao cadastro do edital.

**Listagem com informações do banco de equivalentes.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Uso do Banco/Teto                     | N/A                      | N/A          | S            | Indica a relação entre o uso atual do banco de equivalentes e o seu teto máximo.
Saldo                                 | N/A                      | N/A          | S            | Saldo atual do banco de equivalentes.
Saldo a Ocupar/Reservado              | N/A                      | N/A          | S            | Salto a ocupar/reservado no banco de equivalentes.
Saldo Previsto                        | N/A                      | N/A          | S            | Saldo final previsto para o banco de equivalentes.

N/A= Não se Aplica; E = Entrada; S= Saída; E/S= Entrada e Saída; Obrigatório: Sim = S; Não = N.

## **5. Requisitos Legais**

### RL01  

1. Resolução CONSEPE 110/2018, de 10 de junho de 2008 - Aprova normas de gerenciamento do Banco de Professor-Equivalente da UFRN e critérios para distribuição de vagas docentes.

### RL02

1. Decreto 8259/2014, de 29 de maio de 2014 - Altera o Decreto nº 7.485, de 18 de maio de 2011, que dispõe sobre a constituição de banco de professor-equivalente das universidades federais vinculadas ao Ministério da Educação, e altera o Decreto no 7.312, de 22 de setembro de 2010, que dispõe sobre o banco de professor-equivalente de educação básica, técnica e tecnológica dos Institutos Federais de Educação, Ciência e Tecnologia, vinculados ao Ministério da Educação.

## **6. Regras de Negócio**

#### RN01

1. Não será permitido o cadastro de editais com períodos concomitantes.
1. Apenas as vagas com status: `Vaga Desocupada`, do tipo: `Docente` e cargo: `Magistério Superior` poderão ser reservadas junto ao cadastro do edital.
1. O campo: `Data de Exibição do Parecer` deverá ser cadastrado em um período posterior a data de término das solicitações de vagas docente.
1. Durante o preenchimento do edital, ao menos uma vaga deverá ser reservada junto ao cadastro. Contudo, caso o edital possua o tipo de restrição selecionada: `Edital exclusivo para alterações de Regime de Trabalho de Docente`, não será possível reservar vagas desocupadas junto ao cadastro do edital.
1. Após cadastro do edital, as vagas reservadas terão seu status atualizado para: `Vaga Desocupada/Reservada para Distribuição`.  
1. O saldo do banco de equivalentes se utiliza da seguinte fórmula: `SALDO = TETO DO BANCO DE VAGAS - TOTAL DE VAGAS OCUPADAS`. Neste ponto, o saldo total é calculado em cima da diferença entre o saldo limite do teto de vagas mais atual (referente ao teto de banco mais recente cadastrado) e o total de servidores docentes do magistério superior ativos na instituição (Cada categoria de docente possui um fator de vaga específico no teto de vagas).
1. O saldo total previsto para o banco de equivalentes se utiliza da seguinte fórmula: `SALDO DISPONÍVEL = SALDO - SALDO A OCUPAR/RESERVADO`. Neste ponto, o saldo previsto é calculado em cima da diferença entre o saldo atual e o total de solicitações autorizadas (São consideradas solicitações autorizadas as solicitações que ocupam ou não códigos de vagas).
1. O acesso a operação só será possível, caso haja um teto de banco de vagas vigente no atual momento.

## **7. Documentos Relacionados**
- Glossário do módulo de férias (GLO_SIGRH_Férias.md);
- Mensagens do módulo de férias (MSG_SIGRH_Férias.md).
