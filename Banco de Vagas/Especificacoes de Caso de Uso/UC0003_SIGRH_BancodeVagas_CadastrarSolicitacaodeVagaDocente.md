# **Especificação de Caso de Uso: Cadastrar Solicitação de Vaga Docente**

## **1. Descrição do Caso de Uso**

- Caso de uso responsável por realizar o cadastro das solicitações de vaga docente.

- Durante o período estabelecido no edital de distribuição de vagas, os gestores das unidades acadêmicas (departamentos e/ou unidades acadêmicas especializadas) poderão realizar o cadastro das solicitações de novas vagas docente, apresentando as devidas justificativas e necessidades para cada demanda de reposição de vagas.

## **2. Pré-condições**

- O ator deve estar logado no sistema como gestor/ vice-gestor de unidade para ter acesso a funcionalidade.

## **3. Fluxo de Eventos**

### **3.1. Fluxo Principal**

1. O ator seleciona a opção *Cadastrar Solicitação de Vaga Docente*; **[[BD01](#bd01)]**, **[[RN01.1](#rn01)]**, **[[RN01.2](#rn01)]**, **[MSG005]**, **[MSG006]**
1. O sistema apresenta os campos para cadastro das informações da solicitação; **[[BD02](#bd02)]**, **[[BD03](#bd03)]**, **[[BD04](#bd04)]**, **[[BD06](#bd06)]**, **[[RN01.3](#rn01)]**, **[[RN01.4](#rn01)]**, **[[RN01.5](#rn01)]**
1. O ator informa os dados da demanda e aciona a adição da área de contratação; **[[BD02](#bd02)]**, **[[BD03](#bd03)]**, **[[BD04](#bd04)]**
1. O sistema apresenta as informações da área de contratação; **[[BD05](#bd05)]**
1. O ator informa os dados complementares e aciona o cadastro da solicitação de vaga docente; **[[BD06](#bd06)]**
1. O caso de uso é encerrado.

### **3.2. Fluxo Alternativo:**

#### FA01

**Visualizar área de contratação.**

1. O ator informa os dados da demanda e aciona a adição da área de contratação; **[[BD04](#bd04)]**
1. O sistema apresenta as informações da área de contratação; **[[BD05](#bd05)]**
1. O ator aciona a operação *Visualizar detalhes da área de contratação*; **[[BD05](#bd05)]**
1. O sistema estende o caso de uso **[[Visualizar Detalhes da Area de Contratação](#Visualizar detalhes da área de contratação)]**;
1. O caso de uso é encerrado.

### **3.3. Fluxo de Exceções**

#### FE01

**Acesso realizado fora do período especificado em edital.**

1. O ator acessa a funcionalidade fora do período especificado em edital; **[[RN01.1](#rn01)]**
2. O sistema exibe mensagem de validação; **[MSG005]**
3. O caso de uso é encerrado.

#### FE02

**Acesso de gestor de unidade não-acadêmica.**

1. O ator acessa a funcionalidade sem ser gestor de unidade acadêmica; **[[RN01.2](#rn01)]**
2. O sistema exibe mensagem de validação; **[MSG006]**
3. O caso de uso é encerrado.

## **4. Blocos de Dados**

### BD01

**Formulário para seleção da unidade de responsabilidade.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Unidade                               | SELEÇÃO ÚNICA            | S            | E            | Unidade de responsabilidade. São apresentadas as unidades acadêmicas de responsabilidade do servidor. Campo é exibido apenas nas situações em que o servidor é responsável por mais de uma unidade acadêmica.

### BD02

**Formulário para cadastro das informações da demanda de vaga docente.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Unidade Solicitante                   | N/A                      | N/A          | S            | Unidade solicitante da nova vaga docente.
Tipo de Demanda                       | SELEÇÃO ÚNICA            | S            | E            | Tipo de demanda da nova vaga solicitada.
Servidor                              | AUTOCOMPLETE             | S            | E            | Servidor com carga horária a ser alterada ou servidor removido. Campo é exibido apenas para as solicitações do tipo: `Alteração de Regime de Trabalho` e `Remoção`.
Tipo de Alteração de Regime           | SELEÇÃO ÚNICA            | S            | E            | Tipo de alteração de regime da nova vaga solicitada. Campo é exibido apenas para as solicitações do tipo: `Alteração de Regime de Trabalho`.
Descrição da Demanda                  | TEXTO                    | S            | E            | Descrição da demanda de redistribuição. Campo é exibido apenas para as solicitações do tipo: `Redistribuição`.
Regime de Trabalho                    | SELEÇÃO ÚNICA            | S            | E            | Tipo de regime de trabalho da nova vaga solicitada. Campo não é exibido para as solicitações do tipo: `Alteração de Regime de Trabalho`.
Área do Concurso                      | TEXTO                    | S            | E            | Área de concurso da nova vaga solicitada. Campo não é obrigatório paras as solicitações do tipo: `Alteração de Regime de Trabalho`.
Classe Funcional                      | SELEÇÃO ÚNICA            | S            | E            | Classe funcional da vaga solicitada.
Prioridade                            | NUMÉRICO                 | S            | E            | Prioridade da demanda solicitada.
Grande Área                           | SELEÇÃO ÚNICA            | N            | E            | Grande área da vaga solicitada.
Subárea de Conhecimento               | SELEÇÃO ÚNICA            | S/N          | E            | Subárea de conhecimento da demanda solicitada. Campo é exibido apenas quando o campo: `Grande Área` é selecionado.

### BD03

**Formulário para cadastro dos requisitos de titulação.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Titulação                             | SELEÇÃO ÚNICA            | S/N          | E            | Titulação da nova vaga docente.
Especificação do Requisito            | TEXTO                    | S/N          | E            | Especificação do requisito de titulação da nova vaga docente.

### BD04

**Formulário para cadastro da justificativa/comprovação da demanda.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Tipo de Justificativa para a Demanda  | SELEÇÃO ÚNICA            | S            | E            | Tipo de justificativa para a demanda solicitada.
Tipo de Reposição de Vacância         | SELEÇÃO ÚNICA            | S/N          | E            | Tipo de reposição de vacância para a demanda solicitada. Campo é exibido apenas para o tipo de justificativa: `Reposição de Vacância
Docente`                              | AUTOCOMPLETE             | S/N          | E            | Tipo de reposição de vacância para a demanda solicitada. Campo é obrigatório apenas para os tipo de justificativa: Reposição de Vacância
Justificativa para a Demanda          | TEXTO                    | S            | E            | Justificativa para a demanda solicitada.
Adicionar Arquivo de Comprovação      | ARQUIVO                  | S            | E            | Arquivo de comprovação da demanda solicitada.

### BD05

**Listagem das áreas de contratação adicionadas.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Prior.	                              | N/A                      | N/A          | S            | Prioridade da nova vaga adicionada.
Tipo de Demanda	                      | N/A                      | N/A          | S            | Tipo de demanda da nova vaga adicionada.
Grande Área	                          | N/A                      | N/A          | S            | Grande área da nova vaga adicionada.
Subárea de Conhecimento               | N/A                      | N/A          | S            | Subárea de conhecimento da nova vaga adicionada.
Regime                                | N/A                      | N/A          | S            | Regime de trabalho da nova vaga adicionada.
Subir Uma Posição                     | N/A                      | N/A          | S            | Possibilita o aumento de prioridade da demanda um nível acima.
Descer Uma Posição                    | N/A                      | N/A          | S            | Possibilita a diminuição da prioridade da demanda um nível abaixo.
Início da Listagem                    | N/A                      | N/A          | S            | Possibilita a diminuição da prioridade da demanda um nível abaixo.
Término da Listagem                   | N/A                      | N/A          | S            | Possibilita a diminuição da prioridade da demanda um nível abaixo.
Alterar item                          | N/A                      | N/A          | S            | Possibilita a alteração do item adicionado.
Remover item                          | N/A                      | N/A          | S            | Possibilita a remoção do item adicionado.

### BD06

**Formulário para cadastro dos dados da justificativa da solicitação.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Justificativa Geral                   | TEXTO                    | S            | E            | Justificativa para as demandas solicitadas.
Plano de Redução de Vagas             | TEXTO                    | S/N          | E            | Plano de redução de vagas para as demandas solicitadas.

## **5. Requisitos Legais**

### RL01  

1. Resolução CONSEPE 110/2018, de 10 de junho de 2008 - Aprova normas de gerenciamento do Banco de Professor-Equivalente da UFRN e critérios para distribuição de vagas docentes.

### RL02

1. Decreto 8259/2014, de 29 de maio de 2014 - Altera o Decreto nº 7.485, de 18 de maio de 2011, que dispõe sobre a constituição de banco de professor-equivalente das universidades federais vinculadas ao Ministério da Educação, e altera o Decreto no 7.312, de 22 de setembro de 2010, que dispõe sobre o banco de professor-equivalente de educação básica, técnica e tecnológica dos Institutos Federais de Educação, Ciência e Tecnologia, vinculados ao Ministério da Educação.

## **6. Regras de Negócio**

#### RN01

1. A funcionalidade só estará disponível para acesso durante o período estabelecido no edital de distribuição de vagas.
1. Apenas as unidades categorizadas como acadêmicas poderão realizar o cadastro das solicitações de vagas docente.
1. Para cada demanda adicionada será necessário informar um requisito de titulação, com exceção das solicitações de alteração de regime de trabalho que não exigem requisitos de titulação.  
1. As solicitações do tipo: `Alteração de Regime de Trabalho` só poderão ser realizadas para servidores da unidade de responsabilidade do usuário logado.
1. As solicitações do tipo: `Remoção` só poderão ser realizadas para servidores que não pertencem a unidade de responsabilidade do usuário logado.

## **7. Documentos Relacionados**
- Glossário do módulo de férias (GLO_SIGRH_Férias.md);
- Mensagens do módulo de férias (MSG_SIGRH_Férias.md).
