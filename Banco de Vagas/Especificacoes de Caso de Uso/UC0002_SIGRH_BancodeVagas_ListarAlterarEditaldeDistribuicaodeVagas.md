# **Especificação de Caso de Uso: Listar/Alterar Edital de Distribuição de Vagas**

## **1. Descrição do Caso de Uso**

- Caso de uso responsável por realizar a consulta e alteração do edital de distribuição de vagas docente.

## **2. Pré-condições**

- O ator deve estar logado no sistema como gestor de banco de vagas ou como gestor editais convocação para ter acesso a funcionalidade.

## **3. Fluxo de Eventos**

### **3.1. Fluxo Principal**

1. O ator seleciona a opção *Listar/Alterar Edital de Distribuição de Vagas*;
1. O sistema apresenta a listagem de editais cadastrados; **[[BD01](#bd01)]**
1. O ator seleciona uma das opções disponíveis para acesso; **[[BD01](#bd01)]**, **[[FA01](#fa01)]**, **[[FA02](#fa02)]**, **[[FA03](#fa03)]**, **[[RN01.1](#rn01)]**
1. O sistema processa a funcionalidade selecionada pelo ator; **[[BD02](#bd02)]**, **[[FE01](#fe01)]**
1. O caso de uso é encerrado.

### **3.2. Fluxo Alternativo:**

#### FA01

**Visualizar edital de distribuição de vagas.**

1. O sistema apresenta a listagem de editais cadastrados; **[[BD01](#bd01)]**
1. O ator aciona a operação *Visualizar Edital de Distribuição de Vagas*;  **[[BD01](#bd01)]**
1. O sistema apresenta as informações do edital ativo; **[[BD02](#bd02)]**
1. O caso de uso é encerrado.

#### FA02

**Alterar edital de distribuição de vagas.**

1. O sistema apresenta a listagem de editais ativos; **[[BD01](#bd01)]**
1. O ator aciona a operação *Alterar Edital de Distribuição de Vagas*;  **[[BD01](#bd01)]**
1. O sistema estende o caso de uso **[[Cadastrar Edital de Distribuição de Vagas](#CadastrarEditaldeDistribuiçãodeVagas)]**;
1. O caso de uso é encerrado.

#### FA03

**Remover edital de distribuição de vagas.**

1. O sistema apresenta a listagem de editais ativos; **[[BD01](#bd01)]**
1. O ator aciona a operação *Remover Edital de Distribuição de Vagas*;  **[[BD01](#bd01)]**, **[[RN01.1](#rn01)]**
1. O caso de uso é encerrado.

### **3.3. Fluxo de Exceções**

#### FE01

**Remoção de editais com solicitações de vaga docente ativas.**

1. O ator aciona a remoção de um edital com solicitações de vaga docente ativas; **[[BD01](#bd01)]**,  **[[RN01.1](#rn01)]**
2. O sistema exibe mensagem de validação;
3. O caso de uso é encerrado.

## **4. Blocos de Dados**

### BD01

**Listagem de editais cadastrados.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Descrição                             | N/A                      | N/A          | S            | Descrição do edital cadastrado.
Período de submissão   	              | N/A                      | N/A          | S            | Período de submissão de vagas do edital cadastrado.
Data de Exibição do Parecer           | N/A                      | N/A          | S            | Data de exibição do parecer do edital cadastrado.
Quant. Solicitações	                  | N/A                      | N/A          | S            | Quantitativo de solicitações existentes para o edital.
Visualizar Edital 	                  | IMAGEM                   | N            | E            | Possibilita a visualização do edital cadastrado. **[[FA01](#fa01)]**
Alterar Edital                        | IMAGEM                   | N            | E            | Possibilita a alteração do edital cadastrado. **[[FA02](#fa02)]**
Remover Edital                        | IMAGEM                   | N            | E            | Possibilita a remoção do edital cadastrado. **[[FA02](#fa02)]**

### BD02

**Modal com informações do edital ativo.**

Campo                                 | Tipo                     | Obrigatório  |Entrada/Saída |Observações
------------------------------------- | ------------------------ | -------------|--------------|-------------
Descrição                             | N/A                      | N/A          | S            | Descrição do edital cadastrado.
Período de submissão   	              | N/A                      | N/A          | S            | Período de submissão de vagas do edital cadastrado.
Data de Exibição do Parecer           | N/A                      | N/A          | S            | Data de exibição do parecer do edital cadastrado.
Quant. Solicitações	                  | N/A                      | N/A          | S            | Quantitativo de solicitações existentes para o edital.

N/A= Não se Aplica; E = Entrada; S= Saída; E/S= Entrada e Saída; Obrigatório: Sim = S; Não = N.

## **5. Requisitos Legais**

### RL01

1. Resolução CONSEPE 110/2018, de 10 de junho de 2008 - Aprova normas de gerenciamento do Banco de Professor-Equivalente da UFRN e critérios para distribuição de vagas docentes.

### RL02

1. Decreto 8259/2014, de 29 de maio de 2014 - Altera o Decreto nº 7.485, de 18 de maio de 2011, que dispõe sobre a constituição de banco de professor-equivalente das universidades federais vinculadas ao Ministério da Educação, e altera o Decreto no 7.312, de 22 de setembro de 2010, que dispõe sobre o banco de professor-equivalente de educação básica, técnica e tecnológica dos Institutos Federais de Educação, Ciência e Tecnologia, vinculados ao Ministério da Educação.

## **6. Regras de Negócio**

#### RN01

1. Não será permitida a remoção de editais com solicitações de vagas ativas.

## **7. Documentos Relacionados**
- Glossário do módulo de férias (GLO_SIGRH_Férias.md);
- Mensagens do módulo de férias (MSG_SIGRH_Férias.md).
