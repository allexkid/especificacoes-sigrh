# **Especifica��o de Caso de Uso: Retornar Solicita��o de Vagas Docente**

## **1. Descri��o do Caso de Uso**

- Caso de uso respons�vel pelo retorno das solicita��es de vaga docente.

- Durante o per�odo estabelecido no edital de distribui��o de vagas, os gestores de banco de vagas poder�o realizar o retorno das solicita��es de vaga docente registradas. Ap�s confirma��o do retorno da solicita��o, estas poder�o ser reeditadas pelos gestores de unidade junto ao Portal do Servidor.

## **2. Pr�-condi��es**

- O ator deve estar logado no sistema como gestor de banco de vagas ou como gestor editais convoca��o para ter acesso a funcionalidade.

## **3. Fluxo de Eventos**

### **3.1. Fluxo Principal**

1. O ator seleciona a op��o *Retornar Solicita��o de Vaga Docente*;
1. O sistema apresenta os campos para cadastro das informa��es da solicita��o; **[[BD01](#bd01)]**, **[[BD02](#bd02)]**, **[[BD03](#bd03)]**
1. O ator informa os dados da justificativa e aciona o retorno da solicita��o de vaga docente; **[[BD03](#bd03)]**, **[EMAIL001]**
1. O caso de uso � encerrado.

### **3.2. Fluxo Alternativo:**

N�o se aplica.

### **3.3. Fluxo de Exce��es**

N�o se aplica.

## **4. Blocos de Dados**

### BD01

**Listagem com informa��es do edital de distribui��o de vagas.**

Campo                                 | Tipo                     | Obrigat�rio  |Entrada/Sa�da |Observa��es
------------------------------------- | ------------------------ | -------------|--------------|-------------
Submiss�o realizada para edital aberto entre |  N/A              | N/A          | S            | Prioridade da nova vaga adicionada.
Unidade solicitante                   | N/A                      | N/A          | S            | Tipo de demanda da nova vaga adicionada.
Justificativa da demanda              | N/A                      | N/A          | S            | Grande �rea da nova vaga adicionada.

### BD02

**Listagem com as �reas de contrata��o especificadas.**

Campo                                 | Tipo                     | Obrigat�rio  |Entrada/Sa�da |Observa��es
------------------------------------- | ------------------------ | -------------|--------------|-------------
Prior.	                              | N/A                      | N/A          | S            | Prioridade da vaga adicionada.
Tipo de Demanda	                      | N/A                      | N/A          | S            | Tipo de demanda da vaga adicionada.
Grande �rea	                          | N/A                      | N/A          | S            | Grande �rea da vaga adicionada.
Sub�rea de Conhecimento               | N/A                      | N/A          | S            | Sub�rea de conhecimento da vaga adicionada.
Regime                                | N/A                      | N/A          | S            | Regime de trabalho da vaga adicionada.

### BD03

**Formul�rio para cadastro dos dados do retorno da solicita��o.**

Campo                                 | Tipo                     | Obrigat�rio  |Entrada/Sa�da |Observa��es
------------------------------------- | ------------------------ | -------------|--------------|-------------
Justificativa                         | TEXTO                    | S            | E            | Justificativa de retorno da solicita��o de vagas.

## **5. Requisitos Legais**

### RL01

1. Resolu��o CONSEPE 110/2018, de 10 de junho de 2008 - Aprova normas de gerenciamento do Banco de Professor-Equivalente da UFRN e crit�rios para distribui��o de vagas docentes.

### RL02

1. Decreto 8259/2014, de 29 de maio de 2014 - Altera o Decreto n� 7.485, de 18 de maio de 2011, que disp�e sobre a constitui��o de banco de professor-equivalente das universidades federais vinculadas ao Minist�rio da Educa��o, e altera o Decreto no 7.312, de 22 de setembro de 2010, que disp�e sobre o banco de professor-equivalente de educa��o b�sica, t�cnica e tecnol�gica dos Institutos Federais de Educa��o, Ci�ncia e Tecnologia, vinculados ao Minist�rio da Educa��o.

## **6. Regras de Neg�cio**

#### RN01

1. A funcionalidade s� estar� dispon�vel para acesso durante o per�odo de submiss�o de vagas, estabelecido no edital de distribui��o de vagas.
1. Ap�s realiza��o do retorno da solicita��o de vagas, o status da solicita��o ser� atualizado para: `Retornada`.

## **7. Documentos Relacionados**
- E-mails do m�dulo de f�rias (EMAIL_SIGRH_Ferias.md);
- Gloss�rio do m�dulo de f�rias (GLO_SIGRH_Ferias.md);
- Mensagens do m�dulo de f�rias (MSG_SIGRH_Ferias.md).
