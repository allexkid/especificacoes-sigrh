# **Documento de Mensagens - Módulo Banco de Vagas**

Código da Mensagem | Descrição da Mensagem                          |  Caso de Uso (ID dos Casos de Uso que utilizam a mensagem) | Motivo
------------------ | ---------------------------------------------- | ---------- | -----------
MSG001             | Já existe edital de distribuição de vagas cadastrado para o período informado. | UC0001 | Impedir o cadastro de editais com períodos concomitantes.
MSG002             | A Data de Exibição do Parecer deve ser posterior a Data de Término do período de submissão de vagas docente. | UC0001 | Impedir o cadastro da data de exibição do parecer antes da data término da solicitação de vagas.
MSG003             | Nenhuma vaga foi reservada para o edital. | UC0001 | Impedir o cadastro de editais sem vagas adicionadas.
MSG004             | O edital não pode ser removido pois possui solicitações de vaga docente associadas. | UC0002 | Impedir a remoção de editais com solicitações de vaga docente ativas.
MSG005             | O edital não pode ser removido pois possui solicitações de vaga docente associadas. | UC0002 | Impedir a remoção de editais com solicitações de vaga docente ativas.
