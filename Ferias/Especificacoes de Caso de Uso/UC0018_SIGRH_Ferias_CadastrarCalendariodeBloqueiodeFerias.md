# **Especifica��o de Caso de Uso: Cadastrar Calend�rio de Bloqueio de F�rias**

## **1. Descri��o do Caso de Uso**

- Caso de uso respons�vel por realizar o cadastro do calend�rio de bloqueio de f�rias.

- O calend�rio de bloqueio de f�rias � utilizado quando a institui��o deseja impedir o agendamento das f�rias
por parte dos servidores em per�odos que julgue como indispon�veis a interesse da institui��o.

## **2. Pr�-condi��es**

- O ator deve estar logado no sistema como gestor de f�rias ou como gestor de administra��o de pessoal para ter acesso a funcionalidade.

## **3. Fluxo de Eventos**

### **3.1. Fluxo Principal**

1. O ator seleciona a op��o *Cadastrar Calend�rio de Bloqueio de F�rias*;
1. O sistema apresenta os campos para cadastro das informa��es de bloqueio; **[[BD01](#bd01)]**
1. O ator informa os dados necess�rios e aciona o cadastro do calend�rio de bloqueio das f�rias;
**[[BD01](#bd01)]**, **[[FE01](#fe01)]**, **[MSG002]**
1. O caso de uso � encerrado.

### **3.2. Fluxo Alternativo:**

N�o se aplica.

### **3.3. Fluxo de Exce��es**

#### FE01

**Cadastro de calend�rios concomitantes.**

1. O ator realiza o cadastro de um novo calend�rio concomitante com outro calend�rio; **[[BD01](#bd01)]**, **[[RN01.1](#rn01)]**
2. O sistema exibe mensagem de valida��o; **[MSG002]**
3. O caso de uso � encerrado.

## **4. Blocos de Dados**

### BD01

**Formul�rio para cadastro do calend�rio de bloqueio de f�rias.**

Campo                                 | Tipo                     | Obrigat�rio  |Entrada/Sa�da |Observa��es
------------------------------------- | ------------------------ | -------------|--------------|-------------
Per�odo                               | DATA                     | S            | E            | Per�odo de aplica��o do calend�rio de bloqueio de f�rias. M�scara: DD/MM/AAAA a DD/MM/AAAA.
Exerc�cio                             | NUM�RICO                 | N            | E            | Ano-Exerc�cio de aplica��o do calend�rio de bloqueio de f�rias. M�scara: AAAA.
Categoria Funcional                   | SELE��O �NICA            | N            | E            | Categoria Funcional dos servidores no qual se deseja realizar a aplica��o do calend�rio de bloqueio de f�rias. S�o apresentadas as categorias funcionais ativas.
Unidade                               | UNIDADE                  | N    	      | E            | Unidade de exerc�cio no qual se deseja realizar a aplica��o do calend�rio de bloqueio de f�rias. Dados de Entrada: C�digo da Unidade ou Nome da Unidade.
Considerar Hierarquia da Unidade      | L�GICO                   | N            | E            | Permite o cadastro atrav�s da hierarquia organizacional da unidade de exerc�cio selecionada. O campo � habilitado quando o campo: `Unidade` � selecionado. **[[RN01.2](#rn01)]**
Descri��o                             | TEXTO                    | S            | E            | Campo de descri��o do calend�rio de bloqueio de f�rias. Limite de caracteres: 200.

N/A= N�o se Aplica; E = Entrada; S= Sa�da; E/S= Entrada e Sa�da; Obrigat�rio: Sim = S; N�o = N.

## **5. Requisitos Legais**

N�o se aplica.

## **6. Regras de Neg�cio**

#### RN01 - Regras de neg�cio

1. N�o ser� permitido o cadastro de calend�rios de bloqueio concomitantes em per�odo, dentro de um mesmo ano de exerc�cio, categoria funcional e/ou unidade de exerc�cio. A concomit�ncia tamb�m ocorrer� entre calend�rios com naturezas de bloqueio distintas.
1. O cadastro do calend�rio de bloqueio de f�rias poder� ser estendido junto a hierarquia organizacional das unidades subordinadas a unidade de exerc�cio selecionada. Este comportamento ser� aplicado quando o campo: `Considerar Hierarquia da Unidade` for selecionado.

## **7. Documentos Relacionados**

- Gloss�rio do m�dulo de f�rias (GLO_SIGRH_F�rias.md);
- Mensagens do m�dulo de f�rias (MSG_SIGRH_F�rias.md).
