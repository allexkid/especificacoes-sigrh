# **Especifica��o de Caso de Uso: Consultar Calend�rio de Bloqueio de F�rias**

## **1. Descri��o do Caso de Uso**

- Caso de uso respons�vel por realizar a consulta e altera��o do calend�rio de bloqueio de f�rias.

- O calend�rio de bloqueio de f�rias � utilizado quando a institui��o deseja impedir o agendamento das f�rias
dos servidores em per�odos que julgue como indispon�veis a interesse da institui��o.

## **2. Pr�-condi��es**

- O ator deve estar logado no sistema como gestor de f�rias ou como gestor de administra��o de pessoal para ter
acesso a funcionalidade.

## **3. Fluxo de Eventos**

### **3.1. Fluxo Principal**

1. O ator seleciona a op��o *Consultar Calend�rio de Bloqueio de F�rias*;
1. O sistema apresenta os campos necess�rios para consulta do calend�rio de bloqueio de f�rias; **[[BD01](#bd01)]**
1. O ator informa os dados necess�rios e aciona a consulta do calend�rio de bloqueio das f�rias; **[[BD01](#bd01)]**
1. O caso de uso � encerrado.

### **3.2. Fluxo Alternativo:**

#### FA01

**Altera��o do calend�rio de bloqueio de f�rias.**

1. O ator realiza a consulta do calend�rio de bloqueio de f�rias; **[[BD01](#bd01)]**
2. O sistema apresenta as informa��es; **[MSG002]**
3. O caso de uso � encerrado.

### **3.3. Fluxo de Exce��es**

N�o se aplica.

## **4. Blocos de Dados**

### BD01

**Formul�rio de consulta do calend�rio de bloqueio de f�rias.**

Campo                                 | Tipo                     | Obrigat�rio  |Entrada/Sa�da |Observa��es
------------------------------------- | ------------------------ | -------------|--------------|-------------
Ano                                   | NUM�RICO                 | S            | E            | Per�odo de aplica��o do calend�rio de bloqueio de f�rias. M�scara: DD/MM/AAAA a DD/MM/AAAA.
Per�odo                               | DATA                     | S            | E            | Per�odo de aplica��o do calend�rio de bloqueio de f�rias. M�scara: DD/MM/AAAA a DD/MM/AAAA.
Exerc�cio                             | NUM�RICO                 | N            | E            | Ano-Exerc�cio de aplica��o do calend�rio de bloqueio de f�rias. M�scara: AAAA.
Categoria Funcional                   | SELE��O �NICA            | N            | E            | Categoria Funcional dos servidores no qual se deseja realizar a aplica��o do calend�rio de bloqueio de f�rias. S�o apresentadas as categorias funcionais ativas na institui��o.
Unidade                               | UNIDADE                  | N    	      | E            | Unidade de exerc�cio no qual se deseja realizar a aplica��o do calend�rio de bloqueio de f�rias. Dados de Entrada: C�digo da Unidade ou Nome da Unidade.
Descri��o                             | TEXTO                    | S            | E            | Campo de descri��o do calend�rio de bloqueio de f�rias. Limite de caracteres: 200.

N/A= N�o se Aplica; E = Entrada; S= Sa�da; E/S= Entrada e Sa�da; Obrigat�rio: Sim = S; N�o = N.

## **5. Requisitos Legais**

N�o se aplica.

## **6. Regras de Neg�cio**

#### RN01 - Regras de neg�cio

N�o se aplica.

## **7. Documentos Relacionados**

- Gloss�rio do m�dulo de f�rias (GLO_SIGRH_F�rias.md);
- Mensagens do m�dulo de f�rias (MSG_SIGRH_F�rias.md).
