# **Especifica��o de Caso de Uso: Bloquear Marca��o de Exerc�cio**

## **1. Descri��o do Caso de Uso**

- Caso de uso respons�vel por realizar o cadastro e consulta do bloqueio da marca��o do exerc�cio de f�rias junto aos servidores da institui��o.

- O cadastro do bloqueio da marca��o do exerc�cio de f�rias � utilizado quando a institui��o deseja informar os servidores que perderam o direito de agendar suas f�rias em determinados exerc�cios de f�rias, a crit�rio da administra��o. Os servidores com bloqueio da marca��o do exerc�cio de f�rias ativo ser�o impedidos de cadastrar, de alterar ou de terem suas f�rias homologadas para o respectivo ano-exerc�cio de f�rias bloqueado.

## **2. Pr�-condi��es**

- O ator deve estar logado no sistema como gestor de f�rias ou como gestor de administra��o de pessoal para ter
acesso a funcionalidade.

## **3. Fluxo de Eventos**

### **3.1. Fluxo Principal**

1. O ator seleciona a op��o *Bloquear Marca��o de Exerc�cio*;
1. O sistema apresenta os campos para cadastro das informa��es de bloqueio; **[[BD01](#bd01)]**
1. O ator informa os dados necess�rios e aciona o cadastro do bloqueio da marca��o do exerc�cio de f�rias; **[[BD01](#bd01)]**, **[[FA01](#fa01)]**, **[[FE01](#fe01)]**, **[MSG001]**, **[[RN01.1](#rn01)]**,
**[[RN01.2](#rn01)]**
1. O sistema apresenta as informa��es do cadastro do bloqueio da marca��o do exerc�cio de f�rias;
**[[BD02](#bd02)]**, **[[FA01](#fa01)]**
1. O caso de uso � encerrado.

### **3.2. Fluxo Alternativo:**

#### FA01

**Remover bloqueio da marca��o do exerc�cio de f�rias.**

1. O ator informa um servidor com bloqueio da marca��o do exerc�cio de f�rias ativo; **[[BD01](#bd01)]**
1. O sistema apresenta a possibilidade de remo��o do bloqueio da marca��o do exerc�cio de f�rias para o
servidor informado; **[[BD02](#bd02)]**
1. O ator aciona a remo��o do bloqueio da marca��o do exerc�cio de f�rias; **[[BD02](#bd02)]**
1. O caso de uso � encerrado.

### **3.3. Fluxo de Exce��es**

#### FE01

**Servidor com bloqueio de marca��o do exerc�cio de f�rias ativo.**

1. O ator informa um servidor com bloqueio de marca��o do exerc�cio de f�rias ativo e aciona o cadastro; **[[BD01](#bd01)]**, **[[RN01.2](#rn01)]**
1. O sistema exibe mensagem de valida��o; **[MSG001]**
1. O caso de uso � encerrado.

## **4. Blocos de Dados**

### BD01

**Formul�rio para cadastro do bloqueio da marca��o do exerc�cio de f�rias.**

Campo                                 | Tipo                     | Obrigat�rio  |Entrada/Sa�da |Observa��es
------------------------------------- | ------------------------ | -------------|--------------|-------------
Servidor                              | AUTOCOMPLETE             | S            | E            | Servidor selecionado para cadastro do bloqueio da marca��o do exerc�cio de f�rias. Dados de entrada: Nome do Servidor, Matr�cula SIAPE ou CPF.
Ano do Exerc�cio                      | NUM�RICO                 | S            | E            | Ano-Exerc�cio do servidor no qual se deseja cadastrar o bloqueio da marca��o do exerc�cio de f�rias. M�scara: AAAA.
Motivo                                | TEXTO                    | N            | E            | Justificativa para aplica��o do bloqueio da marca��o do exerc�cio de f�rias. Limite de caracteres: 320.

### BD02

**Listagem de bloqueios ativos para o servidor.**

Campo                                 | Tipo                     | Obrigat�rio  |Entrada/Sa�da |Observa��es
------------------------------------- | ------------------------ | -------------|--------------|-------------
Exerc�cio                             | N/A                      | N/A          | S            | Ano-Exerc�cio do servidor com bloqueio da marca��o do exerc�cio de f�rias ativo.
Motivo                                | N/A                      | N/A          | S            | Justificativa para aplica��o do bloqueio da marca��o do exerc�cio de f�rias.
A��es                                 | IMAGEM                   | N            | E            | �cone de remo��o do bloqueio da marca��o do exerc�cio de f�rias.

N/A= N�o se Aplica; E = Entrada; S= Sa�da; E/S= Entrada e Sa�da; Obrigat�rio: Sim = S; N�o = N.

## **5. Requisitos Legais**

N�o se aplica.

## **6. Regras de Neg�cio**

#### RN01

1. O bloqueio da marca��o do exerc�cio de f�rias s� poder� ser aplicado a servidores ativos da institui��o.
1. N�o ser� permitido o cadastro de mais de um bloqueio de marca��o de f�rias por ano-exerc�cio do servidor.

## **7. Documentos Relacionados**

- Gloss�rio do m�dulo de f�rias (GLO_SIGRH_F�rias.md);
- Mensagens do m�dulo de f�rias (MSG_SIGRH_F�rias.md);
