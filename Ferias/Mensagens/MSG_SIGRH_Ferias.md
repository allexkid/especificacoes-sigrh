# **Documento de Mensagens - M�dulo F�rias**


C�digo da Mensagem | Descri��o da Mensagem                          |  Caso de Uso (ID dos Casos de Uso que utilizam a mensagem) | Motivo
------------------ | ---------------------------------------------- | ---------- | -----------
MSG001             | N�o foi poss�vel cadastrar o bloqueio do exerc�cio. J� existe um bloqueio de exerc�cio para o servidor " + [SIAPE] + [Nome do Servidor] + "em" + [Ano-Exerc�cio] + ".")| UC0001 | Impedir o cadastro de mais de um bloqueio de marca��o de f�rias por exerc�cio do servidor.
MSG002             | Existem outros calend�rios de bloqueio de f�rias cadastrados no sistema cujo per�odo � concomitante ao informado no cadastro. | UC0018 | Impedir o cadastro de calend�rios concomitantes em per�odo, unidade, per�odo, ano-exerc�cio, categoria funcional e/ou unidade de exerc�cio.
